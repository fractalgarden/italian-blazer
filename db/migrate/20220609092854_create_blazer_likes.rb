class CreateBlazerLikes < ActiveRecord::Migration[6.1]
  def change
    create_table :blazer_likes, id: :serial, force: :cascade do |t|
      t.references :creator
      t.references :query
      t.timestamps
    end
  end
end
