class AddCategoryIdToBlazerQueries < ActiveRecord::Migration[6.1]
    def change
      add_reference :blazer_queries, :category
      add_foreign_key :blazer_queries, :blazer_categories, column: :category_id, primary_key: "id"
    end
end
