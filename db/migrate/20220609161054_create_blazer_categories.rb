class CreateBlazerCategories < ActiveRecord::Migration[6.1]
  def change
    create_table :blazer_categories, id: :serial, comment: "Entità Categoria Blazer", force: :cascade do |t|
      t.string "name", limit: 255, null: false, comment: "Nome della categoria blazer"
      t.timestamps
    end
  end
end
