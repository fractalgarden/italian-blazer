module Blazer
  class CategoriesController < BaseController
    before_action :check_is_category_admin

    def index
      @categories = Blazer::Category.all.order(:name)
    end

    def show
      @category = Blazer::Category.find(params[:id])
    end

    def new
      @category = Blazer::Category.new
    end

    def create
      @category = Blazer::Category.new(category_params)

      if @category.save
        redirect_to_list
      else
        render :new, status: :unprocessable_entity
      end
    end

    def edit
      @category = Blazer::Category.find(params[:id])
    end

    def update
      @category = Blazer::Category.find(params[:id])

      if @category.update(category_params)
        redirect_to_list
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @category = Blazer::Category.find(params[:id])
      Blazer::Query.where('category_id = ?', params[:id]).update_all(category_id: nil)
      @category.destroy

      redirect_to_list
    end

    def redirect_to_list
      redirect_to action: "index"
    end

    private

    def category_params
      params.require(:category).permit(:name)
    end

    def check_is_category_admin
      render_forbidden if !is_category_admin
    end

    def render_forbidden
      render plain: "Access denied", status: :forbidden
    end
  end
end

