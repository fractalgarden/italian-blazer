module Blazer
  class Query < Record
    belongs_to :creator, optional: true, class_name: Blazer.user_class.to_s if Blazer.user_class
    belongs_to :category
    has_many :checks, dependent: :destroy
    has_many :dashboard_queries, dependent: :destroy
    has_many :dashboards, through: :dashboard_queries
    has_many :audits
    has_many :likes

    validates :statement, presence: true

    scope :active, -> { column_names.include?("status") ? where(status: ["active", nil]) : all }
    scope :named, -> { where.not(name: "") }

    def to_param
      [id, name].compact.join("-").gsub("'", "").parameterize
    end

    def friendly_name
      name.to_s.sub(/\A[#\*]/, "").gsub(/\[.+\]/, "").strip
    end

    def viewable?(user)
      if Blazer.query_viewable
        Blazer.query_viewable.call(self, user)
      else
        true
      end
    end

    def editable?(user)
      editable = !persisted? || (name.present? && name.first != "*" && name.first != "#") || user == try(:creator)
      editable &&= viewable?(user)
      editable &&= Blazer.query_editable.call(self, user) if Blazer.query_editable
      editable
    end

    def variables
      # don't require data_source to be loaded
      variables = Statement.new(statement).variables
      variables += ["cohort_period"] if cohort_analysis?
      variables
    end

    def cohort_analysis?
      # don't require data_source to be loaded
      Statement.new(statement).cohort_analysis?
    end

    def statement_object
      Statement.new(statement, data_source)
    end

    def category_name
      category ? category.name : "No category"
    end

    def self.include_likes_count
      joins(
        %{
       LEFT OUTER JOIN (
         SELECT likes.query_id, COUNT(*) likes_count
         FROM   blazer_likes likes
         GROUP BY likes.query_id
       ) likes ON likes.query_id = blazer_queries.id
     }
      ).select("COALESCE(likes.likes_count, 0) AS likes_count")
    end

    def self.include_liked(creator_id)
      return select("(0) AS liked") if creator_id.nil?

      joins(
        %{
       LEFT OUTER JOIN (
         SELECT likes.query_id
         FROM   blazer_likes likes
         where likes.creator_id = #{creator_id}
       ) likes_liked ON likes_liked.query_id = blazer_queries.id
     }
      ).select("(CASE COALESCE(likes_liked.query_id, 0) WHEN 0 THEN FALSE ELSE TRUE END) AS liked")
    end

    def self.include_viewed_count
      joins(
        %{
       LEFT OUTER JOIN (
         SELECT audits.query_id, COUNT ( * ) viewed_count
         FROM blazer_audits audits
         GROUP BY audits.query_id
       ) audits ON audits.query_id = blazer_queries.id
     }
      ).select("COALESCE(audits.viewed_count, 0) AS viewed_count")
    end
  end
end
